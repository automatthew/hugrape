walk(document.body);

function walk(node) 
{
	// I stole this function from here:
	// http://is.gd/mwZp7E
	
	var child, next;

	switch ( node.nodeType )  
	{
		case 1:  // Element
		case 9:  // Document
		case 11: // Document fragment
			child = node.firstChild;
			while ( child ) 
			{
				next = child.nextSibling;
				walk(child);
				child = next;
			}
			break;

		case 3: // Text node
			handleText(node);
			break;
	}
}

function handleText(textNode) 
{
	var v = textNode.nodeValue;

	v = v.replace(/\bhug\b/g, "rape");
	v = v.replace(/\bhugs\b/g, "rapes");

	v = v.replace(/\bHug\b/g, "Rape");
	v = v.replace(/\bHugs\b/g, "Rapes");

	v = v.replace(/\bhugged\b/g, "raped");
	v = v.replace(/\bHugged\b/g, "Raped");

	v = v.replace(/\bhugger\b/g, "rapist");
	v = v.replace(/\bHugger\b/g, "Rapist");

	v = v.replace(/\bhuggers\b/g, "rapists");
	v = v.replace(/\bHuggers\b/g, "Rapists");

	v = v.replace(/\bHUG\b/g, "RAPE");
	v = v.replace(/\bHUGS\b/g, "RAPES");

	v = v.replace(/\bMcRapey\b/g, "McHuggy");
	
	textNode.nodeValue = v;
}


